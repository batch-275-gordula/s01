@extends('layouts.app')

@section('content')
    <div class="card">
        <div class="card-body">
            <h2 class="card-title">{{$post->title}}</h2>
            <p class="card-subtitle text-muted">Author: {{$post->user->name}}</p>
            <p class="card-subtitle text-muted">Created at: {{$post->created_at}}</p>
            <p class="card-text">{{$post->content}}</p>
            <p class="card-text mt-3">Likes: {{ $post->likes->count() }} | Comments: {{ $post->comments->count() }}</p>
            @if(Auth::user())
                @if(Auth::id() != $post->user_id)
                    <form class="d-inline" method="POST" action="/posts/{{$post->id}}/like">
                        @method('PUT')
                        @csrf
                        @if($post->likes->contains("user_id", Auth::id()))
                            <button type="submit" class="btn btn-danger">Unlike</button>
                        @else
                            <button type="submit" class="btn btn-success">like</button>

                        @endif
                    </form>
                @else
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#commentModal">Comment</button>
                @endif
            @endif
            <div class="mt-3">
                <a href="/posts" class="card-link">View All Posts</a>
            </div>
        </div>
    </div>


    {{-- Comment Section for s05 Activity --}}

    <h2>Comments:</h2>
    @foreach($post->comments as $comment)
        <div class="card">
            <div class="card-body">
                <h2 class="card-title text-center">{{$comment->content}}</h2>

                <h3 class="text-right">Posted by: {{$comment->user->name}}</h3>
                <p class="card-subtitle text-muted text-right">posted on: {{$comment->created_at}}</p>
            </div>
        </div>
    @endforeach


	{{-- Modal for s05 Activity (Posting comments) --}}

    <div class="modal fade" id="commentModal" tabindex="-1" role="dialog" aria-labelledby="commentModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <form method="POST" action="/posts/{{$post->id}}/comment">
                    @method('PUT')
                    @csrf
                    <div class="modal-header">
                        <h5 class="modal-title" id="commentModalLabel">Leave a Comment</h5>
                        <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" name="post_id" value="{{ $post->id }}">
                        <div class="form-group">
                            <label for="comment_content">Comment:</label>
                            <textarea class="form-control" id="comment_content" name="content" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
