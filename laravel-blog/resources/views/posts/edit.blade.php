<!-- Activity 03 -->
<!-- 
@extends('layouts.app')

@section('content')
	<h1>Edit Post</h1>
	{{-- Form for updating the post --}}
	<form method="POST" action="/posts/{{$post->id}}">
		@method('PUT')
		@csrf
		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" class="form-control" id="title" name="title" value="{{$post->title}}">
		</div>
		<div class="form-group">
			<label for="content">Content</label>
			<textarea class="form-control" id="content" name="content">{{$post->content}}</textarea>
		</div>
		<button type="submit" class="btn btn-primary">Update</button>
	</form>
@endsection -->


<!-- Solution for Activity 3 -->
@extends('layouts.app')

@section('content')
	<form method="POST" action="/posts/{{$post->id}}">
		@csrf
		@method('PUT')
		<div class="form-group">
			<label for="title">Title:</label>
			<input type="text" name="title" id="title" class="form-control" value="{{$post->title}}">
		</div>

		<div class="form-group">
			<label for="content">Content:</label>
			<textarea name="content" id="content" class="form-control">{{$post->content}}</textarea>
		</div>

		<div class="mt-2">
			<button type="submit" class="btn btn-primary">Update Post</button>
		</div>
	</form>
@endsection
