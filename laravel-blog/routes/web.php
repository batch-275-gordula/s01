<?php

use Illuminate\Support\Facades\Route;
// link the PostController file
use App\Http\Controllers\PostController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

// defined a route wherein a view(form) to create a post will be returned to the user.
Route::get('/posts/create', [PostController::class, 'create']);

// define a route wherein form data will be sent via POST method to the /posts URI endpoint.
Route::post('/posts', [PostController::class, 'store']);

// define a route that will return a view containing all the posts
Route::get("/posts", [PostController::class, 'index']);

// Activity for s02
// define a route that will return a view for the welcome page.
Route::get('/', [PostController::class, 'welcome']);

// define a route that will return a view containing only the authenticated user's posts.
Route::get('/myPosts', [PostController::class, 'myPosts']);

// define a route wherein a view showing a specific posts with matching URL parameter ID({}) will be returned to the user.
Route::get('/posts/{id}', [PostController::class, 'show']);

// Activty 03
// Show the form to edit a specific post belonging to the authenticated user
// Route::get('/myPosts/{id}/edit', [PostController::class, 'edit'])->name('myPosts.edit');

// Update a specific post belonging to the authenticated user
// Route::put('/myPosts/{id}', [PostController::class, 'update'])->name('myPosts.update');


// Solution Activity 03
// Define a route that will return an edit form for a specific Post when a GET request is received at the "posts/{id}/edit"
Route::get('/posts/{id}/edit', [PostController::class, 'edit']);


// S04 Discussion
// define a route that will overwrite an existing post with matching URL parameter ID via PUT method
Route::put('/posts/{id}', [PostController::class, 'update']);

// define a route that will delete a post of the matching URl parameter.
// Route::delete('/posts/{id}', [PostController::class, 'destroy']);

// Activity 04
Route::delete('/posts/{id}', [PostController::class, 'archive']);

// S05 Discussion
// define a route that will call the 'like action' when a PUT request is received at the '/posts/{id}/like' endpoint.
Route::put('/posts/{id}/like', [PostController::class, 'likes']);

// S05 Activity
Route::put('/posts/{id}/comment', [PostController::class, 'comments']);


