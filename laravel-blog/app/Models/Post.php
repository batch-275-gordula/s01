<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    // one side of the relationship
    public function user(){
        return $this->belongsTo('App\Models\User');
    }

    // S05 Discussion
    public function likes() {
        return $this->hasMany('App\Models\PostLike');
    }

      // Activity S05
      public function comments() {
        return $this->hasMany('App\Models\PostComment');
    }
}
